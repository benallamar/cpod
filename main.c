//
// Created by Marouane BENALLA on 07/10/2016.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Main function
 */
//Define the main modes on the code
static int MODE_SHOW_IN = 0;
static int MODE_SHOW_OUT = 0;
static int MODE_ERASE = 0;
//We initiate the orgPath and the desPath by null value
static char orgPath[] = "nothing";
static char desPath[] = "nothing";

// Define the new type of structure.
int main(int args, char *argv[]) {
    // Function tp execute the finale code.
    void handleArgs(int, char[]);
    handleArgs(args, *argv);
}

typedef struct StringArray {
    char text[];
    struct StringArray *previous;
    struct StringArray *next;
};

void handleArgs(int args, char *argv[]) {
    void trifich(char[], char[]);
    int fileCount = 0;
    while (--args > 0) {
        if (argv[args][0] == "-" && strlen(argv[args]) == 2) {
            if (argv[args][1] == "i")
                MODE_SHOW_IN = 1;
            else if (argv[args][1] == "o")
                MODE_SHOW_OUT = 1;
            else if (argv[args][1] == "e")
                MODE_ERASE = 1;
        } else {
            if (!fileCount) {
                strcmp(argv[args], orgPath);
                fileCount++;
            } else
                strcmp(argv[args], desPath);

        }
    }
    while (!fileCount) {
        printf("The file name is necessary, please tape the name of your file:\n");
        scanf("%s", orgPath);
        if (strlen(orgPath) > 0) {
            fileCount++;
        }
    }
    if (fileCount == 1) {
        printf("Please tape the name of your destination file:\n");
        scanf("%s", desPath);
        if (strlen(desPath) > 0) {
            strcpy(orgPath, desPath);
        }
        while (MODE_ERASE == 1 && fileCount == 1) {
            printf("Please tape the name of your destination file:\n");
            scanf("%s", desPath);
            if (strlen(desPath) > 0) {
                fileCount++;
            }
        }
    }
    trifich(orgPath, desPath);
}

struct StringArray *extractString(FILE *file) {
    struct StringArray *aOne = malloc(sizeof(struct StringArray));
    struct StringArray *first = aOne;
    if (aOne != NULL) {
        char str[100];
        while (fgets(str, 100, file) != NULL) {
            if (MODE_SHOW_IN) {
                puts(str);
            }
            aOne->text = str;
            struct StringArray *aTwo = malloc(sizeof(struct StringArray));
            if (aTwo != NULL) {
                aOne->next = aTwo;
                aTwo->previous = aOne;
                aTwo = aOne;
            }
        }
    }
    return first;
}

void switchNode(struct StringArray *nodeOne, struct StringArray *nodeTwo) {
    char aux = nodeOne->text;
    strcpy(nodeOne->text, nodeTwo->text);
    strcpy(nodeTwo->text, aux);
}

void orderStringArray(struct StringArray *stringArray) {
    struct StringArray *aux = stringArray;
    while (aux->next != NULL) {
        if (strcmp(aux->next->text, aux->text) > 0) {
            switchNode(aux, aux->next);
            aux = aux->next;
        }
    }
    orderStringArray(stringArray->next);
}

void writeFile(struct StringArray *stringArray, FILE *destFile) {
    fputs(stringArray->text, destFile);
    while (stringArray->next != NULL) {
        stringArray = stringArray->next;
        if (MODE_SHOW_OUT) {
            puts(stringArray->text);
        }
        fputs(stringArray->text, destFile);
    }
}

// Function that handle
void trifich(char originFile[], char destinFile[]) {
    // Open the files
    FILE *ori = fopen(orgPath, "r");
    if (ori == NULL) {
        printf("No file has been found");
    } else {
        //Extract the list
        struct StringArray *stringArray = extractString(ori);
        fclose(ori);
        FILE *des = fopen(desPath, "a");
        // Order the list
        orderStringArray(stringArray);
        //Create the file
        writeFile(stringArray, des);
        fclose(des);
    }
}
